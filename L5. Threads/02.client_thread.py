#!/usr/bin/env python
# coding: utf-8

# In[1]:


# import socket dan sys
import socket
import sys


# In[2]:


# fungsi utama
def main():
    # buat socket
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # tentukan IP server target
    host = "192.168.43.204"
    
    # tentukan por server
    port = 80

    # lakukan koneksi ke server
    try:
        soc.connect((host, port))
    except:
        # print error
        print("Connection error")
        # exit
        sys.exit()
    
    # tampilkan menu, enter quit to exit
    print("Enter 'quit' to exit")
    message = input("Masukkan pesan yang akan dikirim -> ")

    # selama pesan bukan "quit", lakukan loop forever
    while message != 'quit':
        # kirimkan pesan yang ditulis ke server
        soc.sendall(message.encode("utf8"))
        
        # menu (user interface)
        message = input("Masukkan pesan yang akan dikirim -> ")

    # send "quit" ke server
    soc.send(b'--quit--')


# In[3]:


# panggil fungsi utama
if __name__ == "__main__":
    main()


# In[ ]:




